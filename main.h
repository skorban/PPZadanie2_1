#ifndef MAIN_H_
#define MAIN_H_
#include "stdbool.h"
#include <stdio.h>

void wyswietlLiczbe1(int32_t liczba);
void wyswietlLiczbe2(int32_t *wsk_liczba);

void wyswietlText(char *text);
void wyswietlZnak(char *text, uint8_t index);
bool porownajZnaki(char *txt1, uint8_t idx1, char *txt2, uint8_t idx2);
int16_t suma(int8_t a, int8_t);
int16_t roznica(int8_t a, int8_t);
int16_t iloczyn(uint8_t a, uint8_t b);
int16_t iloraz(uint8_t a, uint8_t b);
uint32_t poleProstokata(uint8_t a, uint8_t b);
uint32_t moduloAprzezB(uint16_t a, uint8_t b);
float poleTrojkata(uint8_t a, uint8_t h);
uint32_t silniaLiczby(uint8_t a);
uint32_t potegaLiczby(uint8_t a, uint8_t b);
uint8_t liczbaPowtorzenZnaku(char *text, char znak);
uint8_t znajdzZnak(char *text, char znak);

#endif // MAIN_H

