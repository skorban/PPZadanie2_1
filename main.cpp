#include <iostream>
#include <stdlib.h>
#include "stdint.h"
#include "main.h"
#include <math.h>
#include <string.h>

/*
 * === Wersja ostateczna, poprawiona o komentarze i rozwiązana ===
 *
 * 1. Pliki są zapisane jako cpp przez kompilator (JetBrains CLion), jako że Pracuję na środowisku unixowym, dokładnie
 * na Macu, dlatego znalezienie kompilator który będzie działa poprawnie
 * nie było łatwe, na szczęście nabyłem sudencką licencję na produkty JetBrains, i podczas tworzenia projektu
 * w CLion najpewniej automatycznie dobrano cpp.
 *
 * 2. Nie używam namesapce (przestrzeni nazw w zwiazku z tym że uznaje się to obecnie za złą praktykę (zgodnie z
 * opiniami w internecie), najczęściej pojawia się to w kontekście stosowania różnych namespace i nazw funkcji.
 * Czytelność kodu się zmniejsza a zamiana jednej funkcji może powodować że nie odwoła sie ona gdzie trzeba.
 */

int main(int argc, char ** argv)
{
    //Zadanie 1:
    int a;
    int b = 0;
    a = b++;
    /* Warości obu zmiennych wynoszą "0". W linijce 11 dochodzi do przypisania wartości a wartości b(0),
     *a następnie podniesienie wartości b o 1, dlatego to wartość b zostaje zwiększona i uzyskujemy wartości a(0)
     * oraz b (10). Gdyby zamiast "b++" użyto "++b" wtedy najpierw powiększono by wartość b o 1 a nstępnie
     * przyosano tą wartość do a.
     */

    //Zadanie 2: Zadanie wykonano. Podano wartość tekstową która jest zmienną typu char
    std::cout << "\nZadanie 2: "; //3. cout jest typowe dla C++, zgodnie z komentarzem dalej będę stosował printf().
    wyswietlText("Witaj Świecie! // Hello World!");

    //Zadanie 3:
    b = 25; //ustawiam wartosc b na 25 dla lepszej widocznosci zadania
    int32_t *p = &b; //tworze pointer który musi się odwoływac do adresu zmiennej stąd '&' poprzedający b
    printf("\nZadanie 3: ");
    wyswietlLiczbe1(13);
    wyswietlLiczbe2(p);
    /*
     * zmienna będąca pointerem (oznaczenie*) jest wyszukiwana po adresie w pamięci, więc nasza zmienna jest szuakna
     * dokłądniej niż w przypadku zwykłej zmiennej, któ®ej adresy w pamięci moga być różne, albo gdy mamy kilka
     * zmiennych o tej samej wartości to ich adresy są różne - na może zależeć na konkretnej z pewnym adresie
     */

    //Zadanie 4:
    printf("\nZadanie 4: ");
    wyswietlZnak("Hello World", 5);
    /*
     * Funkcja wymaga dwóch argumentów - tekstu oraz indexu. Tekst jest dowolony i jest zmienną typu char,
     * podczas gdy index jest zmienną typu int. Funkcja przyjmuje wartości wpisanego tekstu oraz podanego indexu,
     * sprawdza jaki znak znajduje się w miejscu wskazanym przez index a następnie za pomoca funkcji z bibliotek,
     * wypisuje tekst wskazujący znak.
     */

   //Zadanie 5:
   printf("\nZadanie 5: ");
   char tab1[] = {"To jest mój pierwszy tekst"};
   char tab2[] = {"Moja druga linijka"};
   char wynik1 = porownajZnaki(tab1, 2, tab2, 2); //Tworzę dwie tablice znaków o nazwach wynik1 i wynik2
   char wynik2 = porownajZnaki(tab1, 6, tab2, 6);

    if(wynik1 != 0) //Wskazuję możliwe rezolucje dla dwóch przypadków (dwóch różnych znaków które porównuję)
    {
        printf("\nZnaki numer 2 są identyczne");
    }
    else
    {
        printf("\nZnaki numer 2 są różne");
    }

    if(wynik2 != 0)
    {
        printf("\nZnaki numer 6 są identyczne");
    }
    else
    {
        printf("\nZnaki numer 6 są różne");
    }

    //Zadanie 6:
    printf("\nZadanie 6 (Operacje na liczbach 5 i 3): ");
    suma(5,3);
    roznica(5,3);
    iloczyn(5,3);
    iloraz(5,3);

    //Zadanie 7:
    printf("\nZadanie 7: ");
    poleProstokata(5,7);
    moduloAprzezB(5,3);

    //Zadanie 8:
    printf("\nZadanie 8: ");
    poleTrojkata(4,3);

    //Zadanie 9:
    printf("\nZadanie 9: ");
    silniaLiczby(4);

    //Zadanie 10: użyto bibliotek istniejacych w C. Wyszedlem z założenia że choć mógłbym to napisać sam to,
    //mądry programista umie czytac dokumentacje i wykorzysta najbardziej optymalny sposob :)
    printf("\nZadanie 10: ");
    potegaLiczby(2, 10);

    //Zadanie 11:
    printf("\nZadanie 11: ");
    liczbaPowtorzenZnaku("Chrząszcz brzmi w trzcinie", 'r');

    //Zadanie 12:
    printf("\nZadanie 12: ");
    znajdzZnak("To jest bardzo, naprawdę bardzo długi tekst\0", 'n');


    /*
    12. Dodaj do programu funkcje znajdzZnak (funkcja szuka pierwszego wystąpienia znaku 'znak' w tablicy znakow
     'text' i zwraca położenie tego znaku w tablicy.
     */

    // system("pause"); Wyrzuciłem z kodu ponieważ na codzień pracuję w środowisku unix (mac) gdzie pause nie działa

    return 0;
}

//Zadanie 2, funkcja
void wyswietlText(char *text) //funkcja jest void tak jak w oryginalnym pliku. aby zwracała wynik powinno być "char"
{
    printf("\n%s", text);
}

//Zadanie 3, funkcja
void wyswietlLiczbe1(int32_t liczba)
{
    printf("\nLiczba 1: %i", liczba);
}

void wyswietlLiczbe2(int32_t *wsk_liczba)
{
    printf("\nLiczba 2: %d", *wsk_liczba);
}

//Zadanie 4, funkcja
void wyswietlZnak(char *text, uint8_t index)
{
    char *c = text + (index - 1) * sizeof(char) ;
    printf("\nZnak numer %d to : %c", index, *c);
}

//Zadanie 5, funkcja
bool porownajZnaki(char *tab1, uint8_t idx1, char *tab2, uint8_t idx2)

{
    bool wynik = false; // deklaruje i przypisuje wartość do zmiennej typu boolean aby zapobiec potencjalnym błędom
    char *c1 = tab1 + (idx1 - 1) * sizeof(char) ;
    char *c2 = tab2 + (idx2 - 1) * sizeof(char) ; //Chodzi oczywiście o drugi string nie znak
    /*
     * Powyżej wybieram konkretny znak z całej tablicy w tym celu do odpowiedniej zmiennej przypisuje wartość
     * z tablicy "odejmując" nie potrzebne znaki. sizeof pozwala mi określić długość tablicy aby wybrać odpowiedni znak
     * czyli ten który wskazuje (odpowiednio 2 i 6). C liczy od wartosci 1 a nie jak java od 0, więc muszę wyrównać
     * wskazanie poprzez odjęcie 1 w idx
     */

    if(*c1 == *c2) //porównuję znaki które wcześniej przypisałem do zmiennych
    {
        wynik = true;
    }
    else
    {
        wynik = false;
    }
    return wynik;
}

// Zadanie 6, funkcje:
    int16_t suma(int8_t a, int8_t b)
    {
        int sum;
        sum = a + b;
        printf("\nWynik dodawania: %i"); //printf() wyświetla string w "" oraz %i czyli integer.
        return sum;
    }

    int16_t roznica(int8_t a, int8_t b)
    {
        int sub;
        sub = a - b;
        printf("\nWynik odejmowania: %i"); //operuje na jednej zmiennej liczbowej więc nie muszę jej deklarować
        return sub;
    }

    int16_t iloczyn(uint8_t a, uint8_t b)
    {
        int mult = a * b;
        printf("\nWynik mnożenia: %i");
        return mult;
    }

    int16_t iloraz(uint8_t a, uint8_t b)
    {
        int div = a / b;
        if (b == 0)
        {
            printf("Nie można dzielić przez zero!"); //Zgodnie z komentarzem dodałem prosty warunek dt. warości 0
        }
        else
        {
            printf("\nWynik dzielenia: %i");
        return div;
        }
    }

//Zadanie 7, funkcje
uint32_t poleProstokata(uint8_t a, uint8_t b)
{
    int pol = a * b;
    printf("\nPole prostokąta o wymiarach 5 i 7 to: %i cm2");
    return pol;
}

uint32_t moduloAprzezB(uint16_t a, uint8_t b)
{
    int mod = a % b;
    printf("\nReszta z dzielenia wynosi: %i");
    return mod;
}

//Zadanie 8, funkcja
float poleTrojkata(uint8_t a, uint8_t h)
{
    float troj = ((a*h)*0.5);
    printf("\nPole trojkata to: %i cm2");
    return troj;
}

//Zadanie 9, funkcja
uint32_t silniaLiczby(uint8_t a)
{
    int silna = 1;
    for (int i=1; i <= a; i++)
    {
        silna=silna*i;
    }
    printf("\nSilnia wynosi: %d", silna); //konieczne było zdefiniowanie jaki integer ma byc wyswietlany
    return silna;
}

/*  Zadanie 10: Użyto bibliotek istniejacych w C (math.h). Wyszedlem z założenia że choć mógłbym to napisać sam to,
    Dobry programista umie czytac dokumentacje i wykorzysta najbardziej optymany sposob */
uint32_t potegaLiczby(uint8_t a, uint8_t b)
{
    int pot = pow(a, b);
    printf("\nLiczba 2 do potęgi 10 Wynosi: %i");
    return pot;
}

//Zadanie 11, funkcja
uint8_t liczbaPowtorzenZnaku(char *text, char znak)
{
    int ilosc = 0;
    int dlug = strlen(text);

    for (int i = 0; i < dlug; ++i)
    {
        if (text[i] == znak)
            ilosc++;
    }
    printf("\nIlość powtórzeń znaku to %i", ilosc); //konieczne było zdefiniowanie jaki integer ma byc wyswietlany
    return ilosc;
}

//Zadanie 12, funkcja
uint8_t znajdzZnak(char *text, char znak)
{
    printf("\nSzukam znaku: %c", znak);

    int dlug2 = strlen(text);
    for (int i = 0; i < dlug2; ++i)
    {
        if (text[i] == znak)
        {
            printf("\nPierwsze wystąpienie znaku na pozycji: %i", i);
            return i;
        }
        else (text[i] != znak);
        {
            dlug2++;
        }
    }
    return 0;
}


